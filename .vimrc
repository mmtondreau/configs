
  "au BufNewFile *.h call InsertCHHeader()


set nocompatible        " Act like vim not vi
filetype off                  " required

"set the runtime path to include Vundle and initialize
filetype plugin indent on    " required



behave xterm
set viminfo='20,\"500,%	" ' Maximum number of previously edited files for which the marks
			"   are remembered.  
			" " Maximum number of lines saved for each register.
			" % When included, save and restore the buffer list.  If Vim is
			"   started with a file name argument, the buffer list is not
			"   restored.  If Vim is started without a file name argument, the
			"   buffer list is restored from the viminfo file.  Buffers
			"   without a file name and buffers for help files are not written
			"   to the viminfo file.
set history=500		" keep {number} lines of command line history

set printoptions=paper:letter,syntax:n,number:y
                        " printoptions affects the ':hardcopy' command.

" TAB HANDLING, C program formatting:
set tabstop=8		" ts, the spacing of tab stops honored by tab characters
                        "     that appear in the file.
set shiftwidth=3	" sw, number of spaces shifted left and right when
                        "     issuing << and >> commands
set smarttab            " a <Tab> in an indent inserts 'shiftwidth' spaces
set softtabstop=3       "     number of spaces that a tab *pressed by the
                        "     user* is equivalent to
set expandtab           " don't input tabs; replace with spaces. <local to buffer>
set shiftround          " round to 'shiftwidth' for "<<" and ">>"
set magic               " normal pattern-matching mode, avoids portability problems




" see Vim book p 71 for this; first set defaults.
set autoindent          " automatically set the indent of a new line
                        "    (local to buffer)
set nosmartindent       " no clever autoindenting (local to buffer); let
                        "    cindent do it
:autocmd!
filetype on

" Explanations at ":help :filetype-indent-on" and ":help :filetype-plugin-on" (note ":" chars)
filetype plugin on
filetype indent on

" Allow cursor to sit on a "virtual newline" in insert mode
set virtualedit=insert,onemore
autocmd!
autocmd FileType * set formatoptions=tcql
    \ autoindent nocindent nosmartindent comments&
" Formatoptions: 'q' allows formatting with "gq".  'r' automates the middle of
"    a comment.  'o' automates comment formatting with the 'o' or 'O'
"    commands.  'c' wrap comments.  'l' do not break lines in insert mode.
autocmd FileType c,cpp,java,pl,perl :set formatoptions=clqro
    \ autoindent cindent comments=s1:/*,mb:*,ex:*/,://
set autoindent          " automatically set the indent of a new line
                        "    (local to buffer)
set nosmartindent       " no clever autoindenting (local to buffer); let
                        "    cindent do it

autocmd!
autocmd BufWrite *.[ch] call ModifyTime()

function! ModifyTime()
  if &modified == 1
    let current_time = strftime("%a %b %d %X %Z %Y")
	normal mi
	let modified_line_no = search("Last Modified:")
	if modified_line_no != 0 && modified_line_no < 10
          exe "normal f:2l" . strlen(current_time) . "s" . current_time
	  ech "Modified date stamp to " . current_time
	  sleep 500m
	  normal 'i
	endif
  endif
endfunction
" if filetype is recognized as c or cpp, these inform cindent
set cinoptions=:0,p0,t0
set cinwords=if,unless,else,while,until,do,for,switch,case
set formatoptions=tcqr
set cinkeys=0{,0},0),:,0#,!^F,o,O,e
                        " keys that trigger C-indenting in Insert mode
                        "   (local to buffer)

                        " Line wrapping works best when you've set the window width
                        " to the limit of the line length you want.
set wrap                " whether to wrap lines
" Make breaks more obvious
set showbreak=+++\ \  
" set number		" number the lines
set numberwidth=4
set incsearch
set showmatch
set backspace=2         " Backspacing works within the inserted text only
                        " See [:help options] for other things to set here.
set cindent


syntax on

" VIM DISPLAY OPTIONS
set showmode            " show which mode (insert, replace, visual)
set ruler               " show line and column numbers in status line
set title               " show window titles if windowing system allows it
set showcmd             " show commands in status line when typing
set wildmenu            " enhanced command and file completion (with <tab>)

map <C-i> 0mz:set fileformat=unix<cr>:set endofline<cr>A<C-V><Tab><esc>:%s/<C-V><Tab>/  /g<cr>:%s/\s*$//<cr>:nohlsearch<cr>ggVG=`z
:map <F2> :set filetype=c<cr>i/**<cr>@file<cr><lt>pre> CPE 357 Spring 2011<cr>-------------------<cr><cr>Program "describe the program here (without quotes)"<cr><cr>Last Modified: Wed Oct 29 13:39:06 PM PDT 2008<lt>/pre><cr>@author Matthew Tondreau<cr>/<cr><cr>#include <unistd.h><cr>#include <stdlib.h><cr>#include <stdio.h><cr><cr>/** Main entry.<cr>@param argc the number of tokens on the input line.<cr>@param argv an array of tokens.<cr>@return 0 on success, 1-255 on failure<cr>/<cr>int<cr>main(int argc, char *argv[])<cr>{<cr><cr>return EXIT_SUCCESS;<cr><bs>}<cr><esc>kkkka<tab>


:map <F3> :set filetype=c<cr>i/**<cr>@file<cr><lt>pre> CPE 357 Spring 2011<cr>-------------------<cr><cr>Header to "progrma header"<cr><cr>Last Modified: Wed Oct 29 13:39:06 PM PDT 2008<lt>/pre><cr>@author Matthew Tondreau<cr>/<cr><cr>#ifndef _HEADERNAME_H_<cr>#define _HEADERNAME_H_<cr><cr>#include <unistd.h><cr>#include <stdlib.h><cr>#include <stdio.h><cr><cr><cr>#endif

:map <Tab> mz:set fileformat=unix<cr>:set endofline<cr>A<C-V><Tab><esc>:%s/<C-V><Tab>/  /g<cr>:%s/\s*$//<cr>:nohlsearch<cr>ggVG=`z

:ab #d #define
:ab #i #include
:ab #b /**********************************************************************
:ab #e <Space>**********************************************************************
:ab #l /*-------------------------------------------------------------------*/

:ab cmain <cr>int main(int argc, char *argv[]) {<cr>return 0;<cr><bs>}<cr><esc>kkO<tab>	<esc>

" autocmd vimenter * NERDTree
"autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTreeType") && b:NERDTreeType == "primary") | q | endif
"if $VIM_CRONTAB == "true"
"   set nobackup
"   set nowritebackup
"endif
